package main

import "fmt"

type board struct {
	width         int
	height        int
	goal          int
	boards        [][]tile
	score         int
	maxNumber     int
	isMove        bool
	isWin         bool
	isLose        bool
	isGameOver    bool
	excludeCoords []string
}

func (b *board) generateBoard() {
	b.boards = [][]tile{}
	for row := 0; row <= b.height; row++ {
		var rowTiles []tile
		for column := 0; column <= b.width; column++ {
			rowTiles = append(rowTiles, tile{column, row, 0})
		}
		b.boards = append(b.boards, rowTiles)
	}
}

func (b *board) getZeroTiles() []tile {
	var zeroTiles = []tile{}
	for row := 1; row <= b.height; row++ {
		for column := 1; column <= b.width; column++ {
			tile := b.boards[row][column]
			if tile.number == 0 {
				zeroTiles = append(zeroTiles, tile)
			}
		}
	}

	return zeroTiles
}

func (b *board) spawnNumber() {
	spawnNumberBags := []int{2, 2, 2, 2, 4, 2, 2, 2, 2, 2}

	zeroTiles := b.getZeroTiles()
	randIdx := random(0, len(zeroTiles))
	randTile := zeroTiles[randIdx]

	b.boards[randTile.y][randTile.x].setNumber(spawnNumberBags[random(0, len(spawnNumberBags))])
	// fmt.Println("Spawn num: ", randTile.x, randTile.y)
}

func (b *board) renderBoard() string {
	var boardString string
	for row := 1; row <= b.height; row++ {
		for column := 1; column <= b.width; column++ {
			tile := b.boards[row][column]
			boardString += fmt.Sprintf("%-4v ", tile.print())
		}
		boardString += "\n"
	}

	return boardString
}

func (b *board) moveNorth() {
	b.isMove = false
	b.excludeCoords = []string{}

	for row := 1; row <= b.height; row++ {
		for column := 1; column <= b.width; column++ {
			t := b.boards[row][column]
			b.move(t, 0, 1)
		}
	}

	if b.isMove == true {
		b.spawnNumber()
	}
}

func (b *board) moveSouth() {
	b.isMove = false
	b.excludeCoords = []string{}

	for row := b.height; row >= 1; row-- {
		for column := 1; column <= b.width; column++ {
			t := b.boards[row][column]
			b.move(t, 0, -1)
		}
	}

	if b.isMove == true {
		b.spawnNumber()
	}
}

func (b *board) moveEast() {
	b.isMove = false
	b.excludeCoords = []string{}

	for column := b.width; column >= 1; column-- {
		for row := 1; row <= b.height; row++ {
			t := b.boards[row][column]
			b.move(t, -1, 0)
		}
	}

	if b.isMove == true {
		b.spawnNumber()
	}
}

func (b *board) moveWest() {
	b.isMove = false
	b.excludeCoords = []string{}

	for column := 1; column <= b.width; column++ {
		for row := 1; row <= b.height; row++ {
			t := b.boards[row][column]
			b.move(t, 1, 0)
		}
	}

	if b.isMove == true {
		b.spawnNumber()
	}
}

func (b *board) isCoordExcluded(t tile) bool {
	var result bool
	coordString := fmt.Sprintf("%d,%d", t.x, t.y)

	for _, coord := range b.excludeCoords {
		if coord == coordString {
			result = true
			break
		}
	}

	return result
}

func (b *board) excludeCoord(t tile) {
	coordString := fmt.Sprintf("%d,%d", t.x, t.y)

	b.excludeCoords = append(b.excludeCoords, coordString)
}

func (b *board) move(t tile, x int, y int) {
	prevX := t.x + x
	prevY := t.y + y

	if prevX > 0 && prevX <= b.width && prevY > 0 && prevY <= b.height {
		prevTile := b.boards[prevY][prevX]

		if prevTile.number > 0 && t.number > 0 && prevTile.number == t.number {
			if b.isCoordExcluded(t) == false && b.isCoordExcluded(prevTile) == false {
				b.excludeCoord(t)

				b.signMaxNumber(t.number + prevTile.number)
				b.score += t.number + prevTile.number
				b.boards[t.y][t.x].setNumber(t.number + prevTile.number)
				b.boards[prevY][prevX].setNumber(0)

				b.isMove = true
			}
		} else if prevTile.number > 0 && t.number == 0 {
			b.boards[t.y][t.x].setNumber(prevTile.number)
			b.boards[prevY][prevX].setNumber(0)

			b.isMove = true
		}

		nextX := t.x - x
		nextY := t.y - y
		if nextX > 0 && nextX <= b.width && nextY > 0 && nextY <= b.height {
			b.move(b.boards[nextY][nextX], x, y)
		}
	}
}

func (b *board) signMaxNumber(number int) {
	if number > b.maxNumber {
		b.maxNumber = number
	}
}

func (b *board) checkGameOver() {
	b.checkWin()
	if b.isGameOver == false {
		b.checkLose()
	}
}

func (b *board) checkWin() {
	if b.maxNumber == b.goal {
		b.isWin = true
		b.isGameOver = true
	}
}

func (b *board) checkLose() {
	isLose := true
	// vertical check
	for row := 1; row <= b.height-1; row++ {
		for column := 1; column <= b.width; column++ {
			currTile := b.boards[row][column]
			nextTile := b.boards[row+1][column]

			if currTile.number > 0 && nextTile.number > 0 && currTile.number == nextTile.number {
				isLose = false
			}
			if currTile.number == 0 {
				isLose = false
			}
		}
	}

	// horizontal check
	for column := 1; column <= b.width-1; column++ {
		for row := 1; row <= b.height; row++ {
			currTile := b.boards[row][column]
			nextTile := b.boards[row][column+1]

			if currTile.number > 0 && nextTile.number > 0 && currTile.number == nextTile.number {
				isLose = false
			}
			if currTile.number == 0 {
				isLose = false
			}
		}
	}

	b.isLose = isLose
	b.isGameOver = isLose
}
