package main

import "fmt"

type tile struct {
	x      int
	y      int
	number int
}

func (t *tile) setNumber(number int) {
	t.number = number
}

func (t *tile) print() string {
	if t.number == 0 {
		return "."
	}
	return fmt.Sprintf("%d", t.number)
}
